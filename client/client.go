package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank/rankservice"
)

var (
	RankClient = rankservice.MustNewClient("middle_rank_service", client.WithHostPorts("127.0.0.1:8886"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
