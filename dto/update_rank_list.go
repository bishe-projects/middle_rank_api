package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type UpdateRankList struct {
	Key        string  `json:"key" binding:"required"`
	Name       string  `json:"name" binding:"required"`
	BusinessId int64   `json:"business_id" binding:"required"`
	Member     string  `json:"member" binding:"required"`
	Delta      float64 `json:"delta" binding:"required"`
}

func (d *UpdateRankList) ConvertToReq() *rank.UpdateRankListReq {
	return &rank.UpdateRankListReq{
		Key:        d.Key,
		BusinessId: d.BusinessId,
		Name:       d.Name,
		Member:     d.Member,
		Delta:      d.Delta,
	}
}
