package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type RankConfig struct {
	Key        string `json:"key" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
	Limit      int64  `json:"limit" binding:"required"`
	IsBigTop   bool   `json:"is_big_top" binding:"required"`
}

func (d *RankConfig) ConvertToRankConfig() *rank.RankConfig {
	return &rank.RankConfig{
		Key:        d.Key,
		BusinessId: d.BusinessId,
		Limit:      d.Limit,
		IsBigTop:   d.IsBigTop,
	}
}
