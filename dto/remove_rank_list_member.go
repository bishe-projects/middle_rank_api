package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type RemoveRankListMember struct {
	Key        string `json:"key" binding:"required"`
	Name       string `json:"name" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
	Member     string `json:"member" binding:"required"`
}

func (d *RemoveRankListMember) ConvertToReq() *rank.RemoveRankListMemberReq {
	return &rank.RemoveRankListMemberReq{
		Key:        d.Key,
		Name:       d.Name,
		BusinessId: d.BusinessId,
		Member:     d.Member,
	}
}
