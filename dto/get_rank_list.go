package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type GetRankList struct {
	Key        string `json:"key" binding:"required"`
	Name       string `json:"name" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
	Start      int64  `json:"start"`
	Length     int64  `json:"length" binding:"required"`
}

func (d *GetRankList) ConvertToReq() *rank.GetRankListReq {
	return &rank.GetRankListReq{
		Key:        d.Key,
		BusinessId: d.BusinessId,
		Name:       d.Name,
		Start:      d.Start,
		Length:     d.Length,
	}
}
