package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type DeleteRankList struct {
	Key        string `json:"key" binding:"required"`
	Name       string `json:"name" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
}

func (d *DeleteRankList) ConvertToReq() *rank.DeleteRankListReq {
	return &rank.DeleteRankListReq{
		Key:        d.Key,
		BusinessId: d.BusinessId,
		Name:       d.Name,
	}
}
