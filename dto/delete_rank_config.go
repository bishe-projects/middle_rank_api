package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type DeleteRankConfig struct {
	Key        string `json:"key" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
}

func (d *DeleteRankConfig) ConvertToReq() *rank.DeleteRankConfigReq {
	return &rank.DeleteRankConfigReq{
		Key:        d.Key,
		BusinessId: d.BusinessId,
	}
}
