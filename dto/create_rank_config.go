package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type CreateRankConfig struct {
	RankConfig *RankConfig `json:"rank_config" binding:"required"`
}

func (d *CreateRankConfig) ConvertToReq() *rank.CreateRankConfigReq {
	return &rank.CreateRankConfigReq{
		Config: d.RankConfig.ConvertToRankConfig(),
	}
}
