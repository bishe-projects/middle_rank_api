package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type GetRankConfigs struct {
	BusinessId int64 `json:"business_id" binding:"required"`
}

func (d *GetRankConfigs) ConvertToReq() *rank.GetRankConfigsReq {
	return &rank.GetRankConfigsReq{
		BusinessId: d.BusinessId,
	}
}
