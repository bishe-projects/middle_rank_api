package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type GetRankConfig struct {
	Key        string `json:"key" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
}

func (d *GetRankConfig) ConvertToReq() *rank.GetRankConfigReq {
	return &rank.GetRankConfigReq{
		Key:        d.Key,
		BusinessId: d.BusinessId,
	}
}
