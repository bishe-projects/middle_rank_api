package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type GetRankLists struct {
	Key        string `json:"key" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
}

func (d *GetRankLists) ConvertToReq() *rank.GetRankListsReq {
	return &rank.GetRankListsReq{
		Key:        d.Key,
		BusinessId: d.BusinessId,
	}
}
