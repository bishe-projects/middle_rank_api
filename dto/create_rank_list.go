package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"

type CreateRankList struct {
	Key        string `json:"key" binding:"required"`
	Name       string `json:"name" binding:"required"`
	BusinessId int64  `json:"business_id" binding:"required"`
}

func (d *CreateRankList) ConvertToReq() *rank.CreateRankListReq {
	return &rank.CreateRankListReq{
		Key:        d.Key,
		BusinessId: d.BusinessId,
		Name:       d.Name,
	}
}
