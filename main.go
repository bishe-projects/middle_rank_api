package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/common_utils/middleware"
	"gitlab.com/bishe-projects/middle_rank_api/handler"
)

func main() {
	r := gin.Default()
	r.Use(middleware.Cors(), middleware.VerifyTokenMiddleware())
	rank := r.Group("/rank")
	{
		rankList := rank.Group("/rankList")
		{
			rankList.POST("/create", handler.CreateRankList)
			rankList.POST("/delete", handler.DeleteRankList)
			rankList.POST("/update", handler.UpdateRankList)
			rankList.POST("/get", handler.GetRankList)
			rankList.POST("/getList", handler.GetRankLists)
			rankList.POST("/removeMember", handler.RemoveRankListMember)
		}
		rankConfig := rank.Group("/rankConfig")
		{
			rankConfig.POST("/create", handler.CreateRankConfig)
			rankConfig.POST("/delete", handler.DeleteRankConfig)
			rankConfig.POST("/get", handler.GetRankConfig)
			rankConfig.POST("/getList", handler.GetRankConfigs)
		}
	}
	r.Run("0.0.0.0:8083")
}
