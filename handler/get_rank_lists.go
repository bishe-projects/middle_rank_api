package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_rank_api/client"
	"gitlab.com/bishe-projects/middle_rank_api/dto"
	"net/http"
)

func GetRankLists(c *gin.Context) {
	var getRankLists dto.GetRankLists
	if err := c.BindJSON(&getRankLists); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.RankClient.GetRankLists(ctx, getRankLists.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
