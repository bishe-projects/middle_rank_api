package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_rank_api/client"
	"gitlab.com/bishe-projects/middle_rank_api/dto"
	"net/http"
)

func CreateRankList(c *gin.Context) {
	var createRankList dto.CreateRankList
	if err := c.BindJSON(&createRankList); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.RankClient.CreateRankList(ctx, createRankList.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
