package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_rank_api/client"
	"gitlab.com/bishe-projects/middle_rank_api/dto"
	"net/http"
)

func GetRankConfig(c *gin.Context) {
	var getRankConfig dto.GetRankConfig
	if err := c.BindJSON(&getRankConfig); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.RankClient.GetRankConfig(ctx, getRankConfig.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
