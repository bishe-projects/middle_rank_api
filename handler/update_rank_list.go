package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_rank_api/client"
	"gitlab.com/bishe-projects/middle_rank_api/dto"
	"net/http"
)

func UpdateRankList(c *gin.Context) {
	var updateRankList dto.UpdateRankList
	if err := c.BindJSON(&updateRankList); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.RankClient.UpdateRankList(ctx, updateRankList.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
